"""
Here a PCA - K means algorithm is implemented in order to find
clusters with node properties
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans


scaler = StandardScaler()

# Data handling
File = pd.read_csv("./OutputFile.csv",index_col = False)
print(list(File))

df = File.iloc[:,1:-1]

scaler.fit(df)
scaled_data = scaler.transform(df)

pca = PCA(n_components = 3)
pca.fit(scaled_data)
x_pca = pca.transform(scaled_data)


# K means algorithm
clustering = KMeans(n_clusters = 10)
clustering.fit(df)

# Plottin principal components with labels
plt.subplot(2,2,1)
plt.scatter(x_pca[:,0],x_pca[:,1], c=clustering.labels_)
plt.xlabel('First Component')
plt.ylabel('Second Componenet')

plt.subplot(2,2,2)
plt.scatter(x_pca[:,1],x_pca[:,2], c=clustering.labels_)
plt.xlabel('Second Component')
plt.ylabel('Third Componenet')

plt.subplot(2,2,3)
plt.scatter(x_pca[:,2],x_pca[:,0], c=clustering.labels_)
plt.xlabel('Third Component')
plt.ylabel('First Componenet')

plt.show()

# Merging dataframes
File['Labels'] = clustering.labels_
print(File['Degree'].describe())

# Cluster output
for label in File['Labels'].unique():
    temp = File[File["Labels"] == label]
    temp['id'].to_csv('Cluster_{}'.format(label), index = False)


