# Network-Data_Mining

Here we try to use a _semantic_ unsupervised clustering on the functional classification of a set of gene taken from an expanded biological network.
After the classification, clusters are carried to homologous V. vinifera genes in order to give a putative function to all uncharacterised vitis proteins.

Biological Data Mining, University of Trento - 2018/2019
